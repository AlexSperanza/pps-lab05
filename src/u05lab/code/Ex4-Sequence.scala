package u05lab.code

object Sequence {

  def sequence[A](a: List[Option[A]]): Option[List[A]] = a.filter(_.isEmpty) match {
    case _ :: _ => None
    case _ => Some(a.map(opt => opt.get).foldLeft(List[A]())((l, v) => l.append(List(v))))
  }
}
