package u05lab.code

import u05lab.code.PerformanceUtils.measure

import java.util.concurrent.TimeUnit
import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import scala.concurrent.duration.FiniteDuration
import scala.util.Random

object PerformanceUtils {
  case class MeasurementResults[T](result: T, duration: FiniteDuration) extends Ordered[MeasurementResults[_]] {
    override def compare(that: MeasurementResults[_]): Int = duration.toNanos.compareTo(that.duration.toNanos)
  }

  def measure[T](msg: String)(expr: => T): MeasurementResults[T] = {
    val startTime = System.nanoTime()
    val res = expr
    val duration = FiniteDuration(System.nanoTime()-startTime, TimeUnit.NANOSECONDS)
    if(!msg.isEmpty) println(msg + " -- " + duration.toNanos + " nanos; " + duration.toMillis + "ms")
    MeasurementResults(res, duration)
  }

  def measure[T](expr: => T): MeasurementResults[T] = measure("")(expr)
}


object CollectionsTest extends App {

  val numOfElements = 1000000
  val rng: Random = new Random()

  println("------------ Linear sequences ------------")
  /* Linear sequences: List, ListBuffer */
  // Creation
  var immutableList: scala.List[Int] = scala.List()
  measure("Immutable list creation"){ immutableList = scala.List.range(1, numOfElements) }
  var mutableList: ListBuffer[Int] = ListBuffer()
  measure("Mutable list creation") { mutableList = ListBuffer.range(1, numOfElements) }
  // Read
  var randomIndex = rng.nextInt(numOfElements)
  measure("Random read immutable list") { immutableList(randomIndex) }
  measure("Random read mutable list") {mutableList(randomIndex) }
  // Update
  randomIndex = rng.nextInt(numOfElements)
  measure("Random update mutable list") { mutableList(randomIndex) = -1 }
  // Delete
  randomIndex = rng.nextInt(numOfElements)
  measure("Random delete mutable list") { mutableList.remove(randomIndex) }

  println("------------ Indexed sequences ------------")
  /* Indexed sequences: Vector, Array, ArrayBuffer */
  // Creation
  var immutableVector: scala.Vector[Int] = scala.Vector()
  measure("Immutable vector creation") { immutableVector = scala.Vector.range(1, numOfElements) }
  var mutableVector: ArrayBuffer[Int] = ArrayBuffer()
  measure("Mutable vector creation") {mutableVector = ArrayBuffer.range(1, numOfElements) }
  // Read
  randomIndex = rng.nextInt(numOfElements)
  measure("Random read immutable list") { immutableVector(randomIndex) }
  measure("Random read mutable list") {mutableVector(randomIndex) }
  // Update
  randomIndex = rng.nextInt(numOfElements)
  measure("Random update mutable list") { mutableVector(randomIndex) = -1 }
  // Delete
  randomIndex = rng.nextInt(numOfElements)
  measure("Random delete mutable list") { mutableVector.remove(randomIndex) }

  println("------------ Sets ------------")
  /* Sets */
  // Creation
  var immutableSet: Set[String] = Set()
  measure("Immutable set creation") { immutableSet = (1 to numOfElements).map(i => i.toString).toSet }
  var mutableSet: scala.collection.mutable.Set[String] = scala.collection.mutable.Set()
  measure("Mutable set creation") {mutableSet = scala.collection.mutable.Set(); ((1 to numOfElements).map(i => i.toString).foreach(s => mutableSet.add(s))) }
  // Read
  randomIndex = rng.nextInt(numOfElements)
  measure("Random read immutable list") { immutableSet(randomIndex.toString) }
  measure("Random read mutable list") {mutableSet(randomIndex.toString) }
  // Update
  randomIndex = rng.nextInt(numOfElements)
  measure("Random update mutable list") { mutableSet.remove(randomIndex.toString); mutableSet.add((-1).toString) }
  // Delete
  randomIndex = rng.nextInt(numOfElements)
  measure("Random delete mutable list") { mutableSet.remove(randomIndex.toString) }

  println("------------ Maps ------------")
  /* Maps */
  // Creation
  var immutableMap: Map[String, Int] = Map()
  measure("Immutable map creation") { immutableMap = (1 to numOfElements).map(i => (i.toString, i)).toMap }
  var mutableMap: scala.collection.mutable.Map[String, Int] = scala.collection.mutable.Map[String, Int]()
  measure("Mutable map creation") {mutableMap = scala.collection.mutable.Map(); (1 to numOfElements).foreach(i => mutableMap.put(i.toString, i)) }
  // Read
  randomIndex = rng.nextInt(numOfElements)
  measure("Random read immutable list") { immutableMap(randomIndex.toString) }
  measure("Random read mutable list") { mutableMap(randomIndex.toString) }
  // Update
  randomIndex = rng.nextInt(numOfElements)
  measure("Random update mutable list") { mutableMap.put(randomIndex.toString, -1) }
  // Delete
  randomIndex = rng.nextInt(numOfElements)
  measure("Random delete mutable list") { mutableMap.remove(randomIndex.toString) }

  /* Comparison */
  import PerformanceUtils._
  val lst = (1 to 1000000).toList
  val vec = (1 to 1000000).toVector
  assert( measure("lst last"){ lst.last } > measure("vec last"){ vec.last } )
}