package u05lab.code

object ExamsManager extends App {

  object ExamResultType extends Enumeration {
    val Retired, Failed, Succeeded = Value
  }

  trait ExamResult {
    def getResult: ExamResultType.Value
    def getEvaluation: Option[Int]
    def cumLaude: Boolean
  }

  trait ExamResultFactory {
    def failed: ExamResult
    def retired: ExamResult
    def succeededCumLaude: ExamResult
    def succeeded(evaluation: Int): ExamResult
  }

  trait ExamManager {

    def createNewCall(call: String): Unit

    def addStudentResult(call: String, student: String, result: ExamsManager.ExamResult): Unit

    def getAllStudentsFromCall(call: String): Set[String]

    def getEvaluationsMapFromCall(call: String): Map[String, Int]

    def getResultsMapFromStudent(student: String): Map[String, String]

    def getBestResultFromStudent(student: String): Option[Int]
  }

  object ExamResult {
    def apply(result: ExamResultType.Value, evaluation: Option[Int], cumLaude: Boolean): ExamResult = new ExamResultImpl(result, evaluation, cumLaude)
  }

  private class ExamResultImpl(val resultType: ExamResultType.Value, val evaluation: Option[Int], val isCumLaude: Boolean) extends ExamResult {
    override def getResult: ExamResultType.Value = resultType

    override def getEvaluation: Option[Int] = evaluation

    override def cumLaude: Boolean = isCumLaude

    override def toString: String = resultType.toString + (evaluation match {
      case Some(value) => "("+value+ cumLaudeString(isCumLaude) +")"
      case None => ""
    })

    private def cumLaudeString(cumLaude: Boolean): String = if (cumLaude) { "L" } else { "" }
  }

  object ExamResultFactory {
    def apply():ExamResultFactory = new ExamResultFactoryImpl()
  }

  private class ExamResultFactoryImpl extends ExamResultFactory {
    override def failed: ExamResult = ExamResult(ExamResultType.Failed, Option.empty, cumLaude = false)

    override def retired: ExamResult = ExamResult(ExamResultType.Retired, Option.empty, cumLaude = false)

    override def succeededCumLaude: ExamResult = ExamResult(ExamResultType.Succeeded, Option.apply(30), cumLaude = true)

    override def succeeded(evaluation: Int): ExamResult = if (evaluation >= 18 && evaluation <= 30) {
      ExamResult(ExamResultType.Succeeded, Option.apply(evaluation), cumLaude = false)
    } else {
      throw new IllegalArgumentException
    }
  }

  object ExamManager {
    def apply(): ExamManager = new ExamManagerImpl
  }

  private class ExamManagerImpl extends ExamManager {

    private var tupleList:  Seq[(String, String, ExamResult)] = Seq()
    private var createdCalls: Seq[String] = Seq[String]()

    override def createNewCall(call: String): Unit = if (createdCalls.contains(call)) {
      throw new IllegalArgumentException
    } else {
      createdCalls = createdCalls :+ call
    }

    override def addStudentResult(call: String, student: String, result: ExamResult): Unit = if (tupleList.exists(t => t._1 == call && t._2 == student)) {
      throw new IllegalArgumentException
    } else {
      tupleList = tupleList :+ ((call, student, result))
    }

    override def getAllStudentsFromCall(call: String): Set[String] = tupleList.filter(t => t._1 == call).map(t => t._2).toSet

    override def getEvaluationsMapFromCall(call: String): Map[String, Int] = tupleList.filter(t => t._1 == call)
                                                                                      .filter(t => t._3.getEvaluation.isDefined)
                                                                                      .map(t => (t._2, t._3.getEvaluation.get))
                                                                                      .toMap

    override def getResultsMapFromStudent(student: String): Map[String, String] = tupleList.filter(t => t._2 == student)
                                                                                           .map(t => (t._1, t._3.toString))
                                                                                           .toMap

    override def getBestResultFromStudent(student: String): Option[Int] = tupleList.filter(t => t._2 == student)
                                                                                   .map(t => t._3.getEvaluation)
                                                                                   .max((a,b) => OptionIntCompare(a, b))

    private object OptionIntCompare extends Ordering[Option[Int]] {
      def apply(x: Option[Int], y: Option[Int]): Int = this.compare(x, y)

      override def compare(x: Option[Int], y: Option[Int]): Int = (x, y) match {
        case (Some(x), Some(y)) => x.compare(y)
        case (None, Some(_)) => -1
        case (Some(_), None) => 1
        case (None, None) => 0
      }
    }
  }

}