package u05lab

import org.junit.jupiter.api.Assertions.{assertEquals, assertThrows}
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.function.Executable
import u05lab.code.List
import u05lab.code.List.nil


class Ex1ListTest {

  @Test
  def testZipRight(): Unit = {
    val list: List[String] = "a" :: "b" :: "c" :: nil
    val pairedList = list.zipRight
    assertEquals(List(("a", 0), ("b", 1), ("c", 2)), pairedList)
  }

  @Test
  def testEmptyZipRight(): Unit = {
    val list: List[String] = nil
    val pairedList = list.zipRight
    assertEquals(nil[(String, Int)], pairedList)
  }

  @Test
  def testPartition(): Unit = {
    val list = 0 :: -1 :: 2 :: -3 :: nil
    val predicate: Int => Boolean = _ >= 0
    val partitionResult = list.partition(predicate)
    assertEquals(0 :: 2 :: nil, partitionResult._1)
    assertEquals(-1 :: -3 :: nil, partitionResult._2)
  }

  @Test
  def testUnevenPartition(): Unit = {
    val list = 0 :: 1 :: 2 :: 3 :: nil
    val predicate: Int => Boolean = _ >= 0
    val partitionResult = list.partition(predicate)
    assertEquals(nil, partitionResult._2)
  }

  @Test
  def testSpan(): Unit = {
    val list = 0 :: 1 :: 2 :: -3 :: 4 :: 5 :: nil
    val predicate: Int => Boolean = _ < 0
    val spanResult = list.span(predicate)
    assertEquals(0 :: 1 :: 2 :: nil, spanResult._1)
    assertEquals(-3 :: 4 :: 5 :: nil, spanResult._2)
  }

  @Test
  def testReduce(): Unit = {
    val list = 1 :: 2 :: 3 :: 4 :: nil
    val operation: (Int, Int) => Int = _ + _
    assertEquals(10, list.reduce(operation))
  }

  @Test
  def testReduceOnEmptyList(): Unit = {
    val list: List[String] = nil
    val operation: (String, String) => String = _+_
    assertThrows(classOf[UnsupportedOperationException], () => { list.reduce(operation) })
  }

  @Test
  def testTakeRight(): Unit = {
    val list = 0 :: 1 :: 2 :: 3 :: 4 :: nil
    assertEquals(3 :: 4 :: nil, list.takeRight(2))
  }

  @Test
  def testCollect(): Unit = {
    val list = 0 :: 1 :: 2 :: 3 :: 4 :: nil
    val partialFunction: PartialFunction[Int, Char] = {
      case e: Int if (e >= 0 && 3 <= 26) => ('a' + e).toChar
    }
    assertEquals(list.collect(partialFunction), 'a' :: 'b' :: 'c' :: 'd' :: 'e' :: nil)
  }
}
