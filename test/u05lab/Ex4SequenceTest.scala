package u05lab

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import u05lab.code.Sequence
import u05lab.code.List

class Ex4SequenceTest {

  @Test
  def testSequence(): Unit = {
    val list: List[Option[Int]] = List(Some(1), Some(2), Some(3))
    assertEquals(Some(List[Int](1, 2, 3)), Sequence.sequence(list))
  }

  @Test
  def testEmptySequence(): Unit = {
    val list: List[Option[Int]] = List.nil
    assertEquals(Some(List.nil), Sequence.sequence(list))
  }

  def testWrongSequence(): Unit = {
    val list: List[Option[Int]] = List(Some(0), Some(1), None)
    assertEquals(None, Sequence.sequence(list))
  }
}
