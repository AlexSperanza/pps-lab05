package u05lab

import org.junit.jupiter.api.Assertions.{assertEquals, assertFalse, assertThrows, assertTrue}
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.function.Executable
import u05lab.code.ExamsManager._

class Ex2Test {

  private val erf: ExamResultFactory = ExamResultFactory()
  private val em: ExamManager = ExamManager()

  // verifica base di ExamResultFactory
  @Test
  def testExamResultsBasicBehaviour(): Unit = { // esame fallito, non c'è voto
    assertEquals(ExamResultType.Failed, erf.failed.getResult)
    assertFalse(erf.failed.getEvaluation.isDefined)
    assertFalse(erf.failed.cumLaude)
    assertEquals("Failed", erf.failed.toString)
    // lo studente si è ritirato, non c'è voto
    assertEquals(ExamResultType.Retired, erf.retired.getResult)
    assertFalse(erf.retired.getEvaluation.isDefined)
    assertFalse(erf.retired.cumLaude)
    assertEquals("Retired", erf.retired.toString)
    // 30L
    assertEquals(ExamResultType.Succeeded, erf.succeededCumLaude.getResult)
    assertEquals(Option.apply(30), erf.succeededCumLaude.getEvaluation)
    assertTrue(erf.succeededCumLaude.cumLaude)
    assertEquals("Succeeded(30L)", erf.succeededCumLaude.toString)
    // esame superato, ma non con lode
    assertEquals(ExamResultType.Succeeded, erf.succeeded(28).getResult)
    assertEquals(Option.apply(28), erf.succeeded(28).getEvaluation)
    assertFalse(erf.succeeded(28).cumLaude)
    assertEquals("Succeeded(28)", erf.succeeded(28).toString)
  }

  // verifica eccezione in ExamResultFactory
  @Test
  def optionalTestEvaluationCantBeGreaterThan30(): Unit = {
    assertThrows(classOf[IllegalArgumentException], () => erf.succeeded(32))
  }

  @Test
  def optionalTestEvaluationCantBeSmallerThan18(): Unit = {
    assertThrows(classOf[IllegalArgumentException], () => erf.succeeded(17))
  }

  // metodo di creazione di una situazione di risultati in 3 appelli
  private def prepareExams(): Unit = {
    em.createNewCall("gennaio")
    em.createNewCall("febbraio")
    em.createNewCall("marzo")
    em.addStudentResult("gennaio", "rossi", erf.failed) // rossi -> fallito

    em.addStudentResult("gennaio", "bianchi", erf.retired) // bianchi -> ritirato

    em.addStudentResult("gennaio", "verdi", erf.succeeded(28)) // verdi -> 28

    em.addStudentResult("gennaio", "neri", erf.succeededCumLaude) // neri -> 30L

    em.addStudentResult("febbraio", "rossi", erf.failed) // etc..

    em.addStudentResult("febbraio", "bianchi", erf.succeeded(20))
    em.addStudentResult("febbraio", "verdi", erf.succeeded(30))
    em.addStudentResult("marzo", "rossi", erf.succeeded(25))
    em.addStudentResult("marzo", "bianchi", erf.succeeded(25))
    em.addStudentResult("marzo", "viola", erf.failed)
  }

  // verifica base della parte obbligatoria di ExamManager
  @Test def testExamsManagement(): Unit = {
    this.prepareExams()
    // partecipanti agli appelli di gennaio e marzo
    assertEquals(Set("rossi", "bianchi", "verdi", "neri"), em.getAllStudentsFromCall("gennaio"))
    assertEquals(Set("rossi", "bianchi", "viola"), em.getAllStudentsFromCall("marzo"))
    // promossi di gennaio con voto
    assertEquals(2, em.getEvaluationsMapFromCall("gennaio").size)
    assertEquals(Option.apply(28), em.getEvaluationsMapFromCall("gennaio").get("verdi"))
    assertEquals(Option.apply(30), em.getEvaluationsMapFromCall("gennaio").get("neri"))
    // promossi di febbraio con voto
    assertEquals(2, em.getEvaluationsMapFromCall("febbraio").size)
    assertEquals(Option.apply(20), em.getEvaluationsMapFromCall("febbraio").get("bianchi"))
    assertEquals(Option.apply(30), em.getEvaluationsMapFromCall("febbraio").get("verdi"))
    // tutti i risultati di rossi (attenzione ai toString!!)
    assertEquals(3, em.getResultsMapFromStudent("rossi").size)
    assertEquals(Some("Failed"), em.getResultsMapFromStudent("rossi").get("gennaio"))
    assertEquals(Some("Failed"), em.getResultsMapFromStudent("rossi").get("febbraio"))
    assertEquals(Some("Succeeded(25)"), em.getResultsMapFromStudent("rossi").get("marzo"))
    // tutti i risultati di bianchi
    assertEquals(3, em.getResultsMapFromStudent("bianchi").size)
    assertEquals(Some("Retired"), em.getResultsMapFromStudent("bianchi").get("gennaio"))
    assertEquals(Some("Succeeded(20)"), em.getResultsMapFromStudent("bianchi").get("febbraio"))
    assertEquals(Some("Succeeded(25)"), em.getResultsMapFromStudent("bianchi").get("marzo"))
    // tutti i risultati di neri
    assertEquals(1, em.getResultsMapFromStudent("neri").size)
    assertEquals(Some("Succeeded(30L)"), em.getResultsMapFromStudent("neri").get("gennaio"))
  }

  // verifica del metodo ExamManager.getBestResultFromStudent
  @Test def optionalTestExamsManagement(): Unit = {
    this.prepareExams()
    // miglior voto acquisito da ogni studente, o vuoto..
    assertEquals(Option.apply(25), em.getBestResultFromStudent("rossi"))
    assertEquals(Option.apply(25), em.getBestResultFromStudent("bianchi"))
    assertEquals(Option.apply(30), em.getBestResultFromStudent("neri"))
    assertEquals(Option.empty, em.getBestResultFromStudent("viola"))
  }


  @Test
  def optionalTestCantCreateACallTwice(): Unit = {
    assertThrows(classOf[IllegalArgumentException], () => {
      this.prepareExams()
      em.createNewCall("marzo")
    })
  }

  @Test
  def optionalTestCantRegisterAnEvaluationTwice(): Unit = {
    assertThrows(classOf[IllegalArgumentException], () => {
      this.prepareExams()
      em.addStudentResult("gennaio", "verdi", erf.failed)
    })
  }
}
